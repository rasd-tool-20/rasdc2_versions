create or replace package         RASDC2_VERSIONS is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_VERSIONS generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  function changes(p_log out varchar2) return varchar2;
end;

/
create or replace package body         RASDC2_VERSIONS is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_VERSIONS generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  RECNUMB10                     varchar2(4000);  RECNUMP                       number := 1
;
  ACTION                        varchar2(4000);  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  PAGE                          number := 0
;
  V_CODE                        clob;
  V_FORUM                       clob;
  V_GITD                        clob;
  V_PROJECT                     clob;
  LANG                          varchar2(4000);
  PFORMID                       varchar2(4000);
  PFORM                         varchar2(4000);  GBUTTONSRC                    varchar2(4000) := RASDI_TRNSLT.text('Search',LANG)
;  GBUTTONSAVE                   varchar2(4000) := RASDI_TRNSLT.text('Save',LANG)
;
  GBUTTONSAVE#SET                set_type;  GBUTTONCOMPILE                varchar2(4000) := RASDI_TRNSLT.text('Compile',LANG)
;
  GBUTTONCOMPILE#SET             set_type;  GBUTTONRES                    varchar2(4000) := RASDI_TRNSLT.text('Reset',LANG)
;
  GBUTTONRES#SET                 set_type;  GBUTTONPREV                   varchar2(4000) := RASDI_TRNSLT.text('Preview',LANG)
;
  GBUTTONPREV#SET                set_type;
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  VUSER                         varchar2(4000);
  VLOB                          varchar2(4000);  BTNBACK                       varchar2(4000) := RASDI_TRNSLT.text('Back',LANG)
;
  COMPID                        number;
  HINTCONTENT                   varchar2(4000);
  UNLINK                        varchar2(4000);
  XRFORM                        varchar2(4000);
  B10NAME                       ctab;
  B10VERSION                    ctab;
  B10GIT                        ctab;
  B10DESCRIPTION                cctab;
  B20LINKX                      ctab;
  B20LINKX#SET                   stab;
  B20TITLE                      ctab;
  B20DATEX                      ctab;
  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := '/* Change LOG:

20210914 - Added new UTL_HTTP REQUEST, GIT aded to ACL

20201122 - Added program RASDC_WELCOME

20200928 - Added program RASDC_GIT

20160413 - Added RSS forum feed

20151202 - Included session variables in filters

20141027 - Added footer on all pages

*/';

    return version;

 end;
function showLabel(plabel varchar2, pcolor varchar2 default 'U', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,' ',''),'.',''),lang,replace(this_form,'2','')||'_DIALOG');

end if;



if pcolor is null then



return v__;



else



return '<font color="'||pcolor||'">'||v__||'</font>';



end if;





end;
procedure post_submit_template is

begin

/*

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;

*/



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text('User has no privileges to save data!', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, 'COMPILE_S', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text('From is not generated.', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           'begin ' || v_server || '.c_debug := false;'|| v_server || '.form(' || PFORMID ||

                           ',''' || lang || ''');end;',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text('From is generated.', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || '<br/> - '||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text('Form is generated with compilation error. Check your code.', lang)||'('||sqlerrm||')';



            else

            sporocilo := RASDI_TRNSLT.text('Form is NOT generated - internal RASD error.', lang) || '('||sqlerrm||')<br>'||

                         RASDI_TRNSLT.text('To debug run: ', lang) || 'begin ' || v_server || '.form(' || PFORMID ||

                         ',''' || lang || ''');end;' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, 'COMPILE_E', pcompid);

end;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop;
       end;
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASDC2_VERSIONS',v_clob, systimestamp, '' );
       end;
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end;
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)









 });
$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("'|| RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) ||'");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
        ';
        return v_out;
       end;
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '
.rasdTxB10DESCRIPTION {

    height: 70px;

    display: grid;

    overflow-y: scroll;

    font-size: x-small;

}



#GBUTTONRES_RASD {

 display: none;

}



#GBUTTONPREV_RASD  {

 display: none;

}
        ';
        return v_out;
       end;
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;
  function version return varchar2 is
  begin
   return 'v.1.1.20240228124439';
  end;
  function this_form return varchar2 is
  begin
   return 'RASDC2_VERSIONS';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version );
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else
declare vc varchar2(2000); begin
null;
exception when others then  null; end;
  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB10 varchar2(4000) PATH '$.recnumb10'
  ,RECNUMP number PATH '$.recnump'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,LANG varchar2(4000) PATH '$.lang'
  ,PFORMID varchar2(4000) PATH '$.pformid'
  ,GBUTTONSAVE varchar2(4000) PATH '$.gbuttonsave'
  ,GBUTTONCOMPILE varchar2(4000) PATH '$.gbuttoncompile'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONPREV varchar2(4000) PATH '$.gbuttonprev'
  ,BTNBACK varchar2(4000) PATH '$.btnback'
  ,HINTCONTENT varchar2(4000) PATH '$.hintcontent'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'recnump') > 0 then RECNUMP := r__.RECNUMP; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'lang') > 0 then LANG := r__.LANG; end if;
 if instr(RESTREQUEST,'pformid') > 0 then PFORMID := r__.PFORMID; end if;
 if instr(RESTREQUEST,'gbuttonsave') > 0 then GBUTTONSAVE := r__.GBUTTONSAVE; end if;
 if instr(RESTREQUEST,'gbuttoncompile') > 0 then GBUTTONCOMPILE := r__.GBUTTONCOMPILE; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonprev') > 0 then GBUTTONPREV := r__.GBUTTONPREV; end if;
 if instr(RESTREQUEST,'btnback') > 0 then BTNBACK := r__.BTNBACK; end if;
 if instr(RESTREQUEST,'hintcontent') > 0 then HINTCONTENT := r__.HINTCONTENT; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10GIT varchar2(4000) PATH '$.b10git'
)) jt ) loop
if r__.x__ is not null then
 if instr(RESTREQUEST,'b10git') > 0 then B10GIT(i__) := r__.B10GIT; end if;
i__ := i__ + 1;
end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b20[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B20LINKX varchar2(4000) PATH '$.b20linkx'
)) jt ) loop
if r__.x__ is not null then
 if instr(RESTREQUEST,'b20linkx') > 0 then B20LINKX(i__) := r__.B20LINKX; end if;
i__ := i__ + 1;
end if;
end loop;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMP') then RECNUMP := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('V_CODE') then V_CODE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('V_FORUM') then V_FORUM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('V_GITD') then V_GITD := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('V_PROJECT') then V_PROJECT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('LANG') then LANG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORMID') then PFORMID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORM') then PFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCOMPILE') then GBUTTONCOMPILE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONPREV') then GBUTTONPREV := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VUSER') then VUSER := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VLOB') then VLOB := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('BTNBACK') then BTNBACK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('COMPID') then COMPID := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('HINTCONTENT') then HINTCONTENT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('UNLINK') then UNLINK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('XRFORM') then XRFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10NAME_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10NAME(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10VERSION_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10VERSION(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GIT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10GIT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10DESCRIPTION_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10DESCRIPTION(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20LINKX_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20LINKX(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20TITLE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20TITLE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20DATEX_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20DATEX(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10NAME') and B10NAME.count = 0 and value_array(i__) is not null then
        B10NAME(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10VERSION') and B10VERSION.count = 0 and value_array(i__) is not null then
        B10VERSION(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GIT') and B10GIT.count = 0 and value_array(i__) is not null then
        B10GIT(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10DESCRIPTION') and B10DESCRIPTION.count = 0 and value_array(i__) is not null then
        B10DESCRIPTION(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20LINKX') and B20LINKX.count = 0 and value_array(i__) is not null then
        B20LINKX(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20TITLE') and B20TITLE.count = 0 and value_array(i__) is not null then
        B20TITLE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20DATEX') and B20DATEX.count = 0 and value_array(i__) is not null then
        B20DATEX(1) := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
declare
v_last number :=
B10NAME
.last;
v_curr number :=
B10NAME
.first;
i__ number;
begin
 if v_last <>
B10NAME
.count then
   v_curr :=
B10NAME
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10NAME.exists(v_curr) then B10NAME(i__) := B10NAME(v_curr); end if;
      if B10VERSION.exists(v_curr) then B10VERSION(i__) := B10VERSION(v_curr); end if;
      if B10GIT.exists(v_curr) then B10GIT(i__) := B10GIT(v_curr); end if;
      if B10DESCRIPTION.exists(v_curr) then B10DESCRIPTION(i__) := B10DESCRIPTION(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B10NAME
.NEXT(v_curr);
   END LOOP;
      B10NAME.DELETE(i__ , v_last);
      B10VERSION.DELETE(i__ , v_last);
      B10GIT.DELETE(i__ , v_last);
      B10DESCRIPTION.DELETE(i__ , v_last);
end if;
end;
declare
v_last number :=
B20LINKX
.last;
v_curr number :=
B20LINKX
.first;
i__ number;
begin
 if v_last <>
B20LINKX
.count then
   v_curr :=
B20LINKX
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B20LINKX.exists(v_curr) then B20LINKX(i__) := B20LINKX(v_curr); end if;
      if B20TITLE.exists(v_curr) then B20TITLE(i__) := B20TITLE(v_curr); end if;
      if B20DATEX.exists(v_curr) then B20DATEX(i__) := B20DATEX(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B20LINKX
.NEXT(v_curr);
   END LOOP;
      B20LINKX.DELETE(i__ , v_last);
      B20TITLE.DELETE(i__ , v_last);
      B20DATEX.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B10NAME.count > v_max then v_max := B10NAME.count; end if;
    if B10VERSION.count > v_max then v_max := B10VERSION.count; end if;
    if B10GIT.count > v_max then v_max := B10GIT.count; end if;
    if B10DESCRIPTION.count > v_max then v_max := B10DESCRIPTION.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B10NAME.exists(i__) then
        B10NAME(i__) := null;
      end if;
      if not B10VERSION.exists(i__) then
        B10VERSION(i__) := null;
      end if;
      if not B10GIT.exists(i__) then
        B10GIT(i__) := null;
      end if;
      if not B10DESCRIPTION.exists(i__) then
        B10DESCRIPTION(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if B20LINKX.count > v_max then v_max := B20LINKX.count; end if;
    if B20TITLE.count > v_max then v_max := B20TITLE.count; end if;
    if B20DATEX.count > v_max then v_max := B20DATEX.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B20LINKX.exists(i__) then
        B20LINKX(i__) := null;
      end if;
      if not B20LINKX#SET.exists(i__) then
        B20LINKX#SET(i__).visible := true;
      end if;
      if not B20TITLE.exists(i__) then
        B20TITLE(i__) := null;
      end if;
      if not B20DATEX.exists(i__) then
        B20DATEX(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="103" blockid="">
----put procedure in the begining of trigger;

post_submit_template;


--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B10NAME.delete; end if;
      for i__ in 1..j__ loop
      begin
      B10VERSION(i__) := B10VERSION(i__);
      exception when others then
        B10VERSION(i__) := null;

      end;
      begin
      B10GIT(i__) := B10GIT(i__);
      exception when others then
        B10GIT(i__) := null;

      end;
      begin
      B10DESCRIPTION(i__) := B10DESCRIPTION(i__);
      exception when others then
        B10DESCRIPTION(i__) := null;

      end;
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10NAME(i__) := null;
        B10VERSION(i__) := null;
        B10GIT(i__) := null;
        B10DESCRIPTION(i__) := null;

      end loop;
  end;
  procedure pclear_B20(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B20LINKX.delete; end if;
      for i__ in 1..j__ loop
      begin
      B20LINKX#SET(i__) := B20LINKX#SET(i__);
      exception when others then
        B20LINKX#SET(i__).visible := true;

      end;
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B20LINKX(i__) := null;
        B20TITLE(i__) := null;
        B20DATEX(i__) := null;
        B20LINKX#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB10 := null;
    RECNUMP := 1;
    GBUTTONCLR := 'GBUTTONCLR';
    PAGE := 0;
    V_CODE := null;
    V_FORUM := null;
    V_GITD := null;
    V_PROJECT := null;
    LANG := null;
    PFORMID := null;
    PFORM := null;
    GBUTTONSRC := RASDI_TRNSLT.text('Search',LANG);
    GBUTTONSAVE := RASDI_TRNSLT.text('Save',LANG);
    GBUTTONCOMPILE := RASDI_TRNSLT.text('Compile',LANG);
    GBUTTONRES := RASDI_TRNSLT.text('Reset',LANG);
    GBUTTONPREV := RASDI_TRNSLT.text('Preview',LANG);
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    VUSER := null;
    VLOB := null;
    BTNBACK := RASDI_TRNSLT.text('Back',LANG);
    COMPID := null;
    HINTCONTENT := null;
    UNLINK := null;
    XRFORM := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_B10(0);
    pclear_B20(0);

  null;
  end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      B10NAME.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
select distinct object_name from user_objects o

where o.object_name like 'RASD%'

and o.object_type = 'PACKAGE'

order by 1;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B10NAME(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.

--<post_select formid="103" blockid="B10">
declare

  x1 clob;

  y1 varchar2(100);



begin

execute immediate '

declare

 x clob;

 y varchar2(100);

begin

 :y := '||B10NAME(i__)||'.version(x);

 :o := x;

end;'

using out y1, out x1

;



B10VERSION(i__) := y1;

B10DESCRIPTION(i__) := x1;

exception when others then



declare

  x1 clob;

  y1 varchar2(100);



begin

execute immediate '

declare

 x clob;

 y varchar2(100);

begin

 :y := '||B10NAME(i__)||'.changes(x);

 :o := x;

end;'

using out y1, out x1

;



B10VERSION(i__) := y1;

B10DESCRIPTION(i__) := x1;

exception when others then



B10VERSION(i__) := '';

B10DESCRIPTION(i__) := 'error';



end;





end;



B10DESCRIPTION(i__) := replace(B10DESCRIPTION(i__),chr(10),'<br/>');



if instr(rasd_client.secSuperUsers, VUSER) > 0 then

   B10GIT(i__) := 'rasdc_files.showfile?pfile=pict/gumbgit.jpg';

end if;


--</post_select>
            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B10NAME.delete(1);
          B10VERSION.delete(1);
          B10GIT.delete(1);
          B10DESCRIPTION.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B10(B10NAME.count);
  null; end;
  procedure pselect_B20 is
    i__ pls_integer;
  begin
      B20LINKX.delete;
      B20TITLE.delete;
      B20DATEX.delete;
--<pre_select formid="103" blockid="B20">
declare

     req   UTL_HTTP.REQ;

     resp  UTL_HTTP.RESP;

     value VARCHAR2(1024);

	 v_url_project varchar2(1000) := 'https://sourceforge.net/p/rasd/code-0/feed';

	 v_url_code varchar2(1000) := 'https://sourceforge.net/projects/rasd/rss?path=/';

	 v_url_git varchar2(1000) := 'https://api.github.com/repos/DomenDolar/RASD/commits';

	 v_url_forum varchar2(1000) := 'https://sourceforge.net/p/rasd/discussion/hintsandtipsrasd/feed.rss';

begin

 --   utl_http.set_wallet( rasdi_client.c_WalletLocation , rasdi_client.c_WalletPwd);

 utl_http.set_wallet( '' );



/*    req := UTL_HTTP.BEGIN_REQUEST('https://sourceforge.net/p/rasd/code-0/feed');

    UTL_HTTP.SET_HEADER(req, 'User-Agent', 'Mozilla/4.0');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_LINE(resp, value, TRUE);

      v_project := v_project || value;

    END LOOP;

    UTL_HTTP.END_RESPONSE(resp);

*/

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(v_url_project);

      for i in 1 .. pieces.count loop

        v_project := v_project || pieces(i);



      end loop;

    end;



/*

    req := UTL_HTTP.BEGIN_REQUEST('https://sourceforge.net/projects/rasd/rss?path=/');

    UTL_HTTP.SET_HEADER(req, 'User-Agent', 'Mozilla/4.0');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_LINE(resp, value, TRUE);

      v_code := v_code || value;

    END LOOP;

    UTL_HTTP.END_RESPONSE(resp);

*/

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(v_url_code);

      for i in 1 .. pieces.count loop

        v_code := v_code || pieces(i);



      end loop;

    end;



/*    req := UTL_HTTP.BEGIN_REQUEST('https://sourceforge.net/p/rasd/discussion/hintsandtipsrasd/feed.rss');

    UTL_HTTP.SET_HEADER(req, 'User-Agent', 'Mozilla/4.0');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_LINE(resp, value, TRUE);

      v_forum := v_forum || value;

    END LOOP;

    UTL_HTTP.END_RESPONSE(resp);

*/

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(v_url_forum);

      for i in 1 .. pieces.count loop

        v_forum := v_forum || pieces(i);



      end loop;

    end;



    declare

     req   UTL_HTTP.REQ;

     resp  UTL_HTTP.RESP;

     value VARCHAR2(1024);

    begin

    req := UTL_HTTP.BEGIN_REQUEST(v_url_git);

    UTL_HTTP.SET_HEADER(req, 'User-Agent', 'Mozilla/4.0');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_TEXT(resp, value, 1000);

      v_gitd := v_gitd || value;

     END LOOP;

      UTL_HTTP.END_RESPONSE(resp);

    exception when UTL_HTTP.end_of_body then

      UTL_HTTP.END_RESPONSE(resp);

    end;





/*

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces('https://api.github.com/repos/DomenDolar/RASD/commits');

      for i in 1 .. pieces.count loop

        v_gitd := v_gitd || pieces(i);



      end loop;

    end;

*/







    /*

    select URIFACTORY.getUri('https://sourceforge.net/p/rasd/code-0/feed').getClob() ,

     URIFACTORY.getUri('https://sourceforge.net/projects/rasd/rss?path=/').getClob() ,

     URIFACTORY.getUri('https://sourceforge.net/p/rasd/discussion/hintsandtipsrasd/feed.rss').getClob()

     into v_project, v_code , v_forum

    from dual;

    */

  exception when others then

  v_project  := '<?xml version="1.0" encoding="utf-8"?>

<rss><channel><item><title>RSS feed is not connected ('||sqlerrm||') - show in browser</title><link>'||v_url_project||'</link><pubDate> </pubDate></item></channel></rss>';

  v_code  := '<?xml version="1.0" encoding="utf-8"?><rss><channel><item><title>Code SourceForge</title><link>'||v_url_code||'</link><pubDate> </pubDate></item><item><title>Code GIT</title><link>'||v_url_git||'</link><pubDate> </pubDate></item></channel></rss>';

  v_forum  := '<?xml version="1.0" encoding="utf-8"?>

<rss><channel><item><title>RSS forum feed is not connected ('||sqlerrm||') - show in browser</title><link>'||v_url_forum||'</link><pubDate> </pubDate></item></channel></rss>';

end;
--</pre_select>
      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
--<SQL formid="103" blockid="B20">
SELECT
LINKX,
TITLE,
DATEX from (

select

  0 vr,

  'D - '||extractvalue(value(a), '//item/title' ) title,

  extractvalue(value(a),'//item/link') linkx,

  extractvalue(value(a),'//item/pubDate') datex

from table (xmlsequence

           (extract( xmltype( v_forum

              )

              , '//rss/channel/item'

           ))) a

where rownum < 5

union all

SELECT 1 vr, 'G - '||nvl(title,'NaN') title, linkx, datex

FROM

JSON_TABLE(v_gitd , '$[*]'

COLUMNS

  (title VARCHAR2(100) PATH '$.commit.message',

   linkx VARCHAR2(100) PATH '$.html_url',

   datex VARCHAR2(100) PATH '$.commit.author.date'

   )

  )

where rownum < 5

union all

select

  1 vr,

  'S - '||nvl(extractvalue(value(a), '//item/title' ),'NaN') title,

  extractvalue(value(a),'//item/link') linkx,

  extractvalue(value(a),'//item/pubDate') datex

from table (xmlsequence

           (extract( xmltype( v_project

              )

              , '//rss/channel/item'

           ))) a

where rownum < 5

union all

select

 2 vr,

  'F - '||extractvalue(value(a), '//item/title' ) title,

  extractvalue(value(a),'//item/link') linkx,

  extractvalue(value(a),'//item/pubDate') datex

from table (xmlsequence

           (extract( xmltype( v_code

              )

              , '//rss/channel/item'

           ))) a

where rownum < 3

)
--</SQL>
;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B20LINKX(i__)
           ,B20TITLE(i__)
           ,B20DATEX(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.

--<post_select formid="103" blockid="B20">
B20LINKX#SET(i__).custom := 'href="'||B20LINKX(i__)||'" target="_blank"';
--</post_select>
            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B20LINKX.delete(1);
          B20TITLE.delete(1);
          B20DATEX.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B20(B20LINKX.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 then
      pselect_B10;
    end if;
    if  nvl(PAGE,0) = 0 then
      pselect_B20;
    end if;

  null;
 end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10NAME.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B20 is
  begin
    for i__ in 1..B20LINKX.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 then
       pcommit_B10;
    end if;
    if  nvl(PAGE,0) = 0 then
       pcommit_B20;
    end if;

  null;
  end;
  procedure formgen_js is
  begin
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB20() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB10 pls_integer;
    iB20 pls_integer;
  function ShowFieldBTNBACK return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldCOMPID return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldERROR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCLR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCOMPILE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONPREV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONRES return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSAVE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSRC return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldHINTCONTENT return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldMESSAGE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldPFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldUNLINK return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldV_CODE return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldV_FORUM return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldV_GITD return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVLOB return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldV_PROJECT return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVUSER return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldWARNING return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldXRFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB20_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function js_link$togit(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'B10GIT%' then
      v_return := v_return || '''!rasdc2_git.webclient?LANG=''+document.RASDC2_VERSIONS.LANG.value+
''&POBJECT='||replace(B10NAME(to_number(substr(name,instr(name,'_',-1)+1))),'"','&quot;')||
'&PGROUP=rasd-tool-20&POWNER='|| USER||'''';
    elsif name is null then
      v_return := v_return ||'''!rasdc2_git.webclient?LANG=''+document.RASDC2_VERSIONS.LANG.value+
''&POBJECT='||replace(B10NAME(1),'"','&quot;')||
'&PGROUP=rasd-tool-20&POWNER='|| USER||'''';
    end if;
    return v_return;
  end;
  procedure js_link$togit(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$togit(value, name));
  end;
  function js_link$back(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'BTNBACK%' then
      v_return := v_return || '''''';
    elsif name is null then
      v_return := v_return ||'''''';
    end if;
    return v_return;
  end;
  procedure js_link$back(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$back(value, name));
  end;
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption><table border="1" id="B10_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10NAME"><span id="B10NAME_LAB" class="label">'|| showLabel('Name','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10VERSION"><span id="B10VERSION_LAB" class="label">'|| showLabel('Version','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10GIT"><span id="B10GIT_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10DESCRIPTION"><span id="B10DESCRIPTION_LAB" class="label">'|| showLabel('Description','',0) ||'</span></td></tr></thead>'); for iB10 in 1..B10NAME.count loop
htp.prn('<tr id="B10_BLOCK_'||iB10||'"><td class="rasdTxB10NAME rasdTxTypeC" id="rasdTxB10NAME_'||iB10||'"><font id="B10NAME_'||iB10||'_RASD" class="rasdFont">'||B10NAME(iB10)||'</font></td><td class="rasdTxB10VERSION rasdTxTypeC" id="rasdTxB10VERSION_'||iB10||'"><font id="B10VERSION_'||iB10||'_RASD" class="rasdFont">'||B10VERSION(iB10)||'</font></td><td class="rasdTxB10GIT rasdTxTypeC" id="rasdTxB10GIT_'||iB10||'"><IMG ONCLICK="javascript: var link=window.open(encodeURI('); js_link$togit(B10GIT(iB10),'B10GIT_'||iB10||'');
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" ');  if  B10GIT(iB10) is not null and '' is not null  then htp.prn('title=""'); end if;
htp.prn(' ID="B10GIT_'||iB10||'_RASD" CLASS="rasdImg" SRC="'||B10GIT(iB10)||'"></IMG></td><td class="rasdTxB10DESCRIPTION rasdTxTypeL" id="rasdTxB10DESCRIPTION_'||iB10||'"><font id="B10DESCRIPTION_'||iB10||'_RASD" class="rasdFont">');  htpClob( ''||B10DESCRIPTION(iB10)||'' );
htp.prn('</font></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_B20_DIV is begin htp.p('');  if  ShowBlockB20_DIV  then
htp.prn('<div  id="B20_DIV" class="rasdblock"><div>
<caption><div id="B20_LAB" class="labelblock"></div></caption><table border="1" id="B20_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20LINKX"><span id="B20LINKX_LAB" class="label">Link</span></td><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20TITLE"><span id="B20TITLE_LAB" class="label">Last changes of RASD</span></td><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20DATEX"><span id="B20DATEX_LAB" class="label">Date of change</span></td></tr></thead>'); for iB20 in 1..B20LINKX.count loop
htp.prn('<tr id="B20_BLOCK_'||iB20||'"><td class="rasdTxB20LINKX rasdTxTypeC" id="rasdTxB20LINKX_'||iB20||'">');  if B20LINKX#SET(iB20).visible then
htp.prn('<a name="B20LINKX_'||iB20||'" id="B20LINKX_'||iB20||'_RASD" ');  if  B20LINKX(iB20) is not null and '' is not null  then htp.prn('title=""'); end if;
htp.prn('');  if B20LINKX#SET(iB20).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B20LINKX#SET(iB20).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B20LINKX#SET(iB20).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B20LINKX#SET(iB20).custom);
htp.prn('');  if B20LINKX#SET(iB20).error is not null then htp.p(' title="'||B20LINKX#SET(iB20).error||'"'); end if;
htp.prn('');  if B20LINKX#SET(iB20).info is not null then htp.p(' title="'||B20LINKX#SET(iB20).info||'"'); end if;
htp.prn('>'||B20LINKX(iB20)||'</a>');  end if;
htp.prn('</td><td class="rasdTxB20TITLE rasdTxTypeC" id="rasdTxB20TITLE_'||iB20||'"><font id="B20TITLE_'||iB20||'_RASD" class="rasdFont">'||B20TITLE(iB20)||'</font></td><td class="rasdTxB20DATEX rasdTxTypeC" id="rasdTxB20DATEX_'||iB20||'"><font id="B20DATEX_'||iB20||'_RASD" class="rasdFont">'||B20DATEX(iB20)||'</font></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| RASDI_TRNSLT.text('RASD versions',LANG)||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');

htp.prn('</head>
<body><div id="RASDC2_VERSIONS_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_VERSIONS_LAB',''|| RASDI_TRNSLT.text('RASD versions',LANG)||'') ||'     </div><div id="RASDC2_VERSIONS_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASDC2_VERSIONS_MENU') ||'     </div>
<form name="RASDC2_VERSIONS" method="post" action="?"><div id="RASDC2_VERSIONS_DIV" class="rasdForm"><div id="RASDC2_VERSIONS_HEAD" class="rasdFormHead"><input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||RECNUMB10||'"/>
<input name="RECNUMP" id="RECNUMP_RASD" type="hidden" value="'||ltrim(to_char(RECNUMP))||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="LANG" id="LANG_RASD" type="hidden" value="'||LANG||'"/>
<input name="PFORMID" id="PFORMID_RASD" type="hidden" value="'||PFORMID||'"/>
');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldBTNBACK  then
htp.prn('<input onclick="javascript: history.go(-1);" name="BTNBACK" id="BTNBACK_RASD" type="button" value="'||BTNBACK||'" class="rasdButton"/>');  end if;
htp.prn('
');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div><div id="RASDC2_VERSIONS_RESPONSE" class="rasdFormResponse"><div id="RASDC2_VERSIONS_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASDC2_VERSIONS_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASDC2_VERSIONS_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASDC2_VERSIONS_BODY" class="rasdFormBody">'); output_B10_DIV; htp.p(''); output_B20_DIV; htp.p('</div><div id="RASDC2_VERSIONS_FOOTER" class="rasdFormFooter">');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'"
 class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldBTNBACK  then
htp.prn('<input onclick="javascript: history.go(-1);" name="BTNBACK" id="BTNBACK_RASD" type="button" value="'||BTNBACK||'" class="rasdButton"/>');  end if;
htp.prn('
');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div></div></form><div id="RASDC2_VERSIONS_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_VERSIONS_BOTTOM',1,instr('RASDC2_VERSIONS_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB20_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>');
    htpp('<form name="RASDC2_VERSIONS" version="'||version||'">');
    htpp('<formfields>');
    htpp('<recnumb10><![CDATA['||RECNUMB10||']]></recnumb10>');
    htpp('<recnump>'||RECNUMP||'</recnump>');
    htpp('<action><![CDATA['||ACTION||']]></action>');
    htpp('<page>'||PAGE||'</page>');
    htpp('<lang><![CDATA['||LANG||']]></lang>');
    htpp('<pformid><![CDATA['||PFORMID||']]></pformid>');
    htpp('<gbuttonsave><![CDATA['||GBUTTONSAVE||']]></gbuttonsave>');
    htpp('<gbuttoncompile><![CDATA['||GBUTTONCOMPILE||']]></gbuttoncompile>');
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>');
    htpp('<gbuttonprev><![CDATA['||GBUTTONPREV||']]></gbuttonprev>');
    htpp('<error><![CDATA['||ERROR||']]></error>');
    htpp('<message><![CDATA['||MESSAGE||']]></message>');
    htpp('<warning><![CDATA['||WARNING||']]></warning>');
    htpp('<btnback><![CDATA['||BTNBACK||']]></btnback>');
    htpp('<hintcontent><![CDATA['||HINTCONTENT||']]></hintcontent>');
    htpp('</formfields>');
    if ShowBlockb10_DIV then
    htpp('<b10>');
  for i__ in 1..
B10NAME
.count loop
    htpp('<element>');
    htpp('<b10name><![CDATA['||B10NAME(i__)||']]></b10name>');
    htpp('<b10version><![CDATA['||B10VERSION(i__)||']]></b10version>');
    htpp('<b10git><![CDATA['||B10GIT(i__)||']]></b10git>');
    htpp('<b10description><![CDATA['||B10DESCRIPTION(i__)||']]></b10description>');
    htpp('</element>');
  end loop;
  htpp('</b10>');
  end if;
    if ShowBlockb20_DIV then
    htpp('<b20>');
  for i__ in 1..
B20LINKX
.count loop
    htpp('<element>');
    htpp('<b20linkx><![CDATA['||B20LINKX(i__)||']]></b20linkx>');
    htpp('<b20title><![CDATA['||B20TITLE(i__)||']]></b20title>');
    htpp('<b20datex><![CDATA['||B20DATEX(i__)||']]></b20datex>');
    htpp('</element>');
  end loop;
  htpp('</b20>');
  end if;
    htpp('</form>');
else
    htpp('{"form":{"@name":"RASDC2_VERSIONS","@version":"'||version||'",' );
    htpp('"formfields": {');
    htpp('"recnumb10":"'||escapeRest(RECNUMB10)||'"');
    htpp(',"recnump":"'||RECNUMP||'"');
    htpp(',"action":"'||escapeRest(ACTION)||'"');
    htpp(',"page":"'||PAGE||'"');
    htpp(',"lang":"'||escapeRest(LANG)||'"');
    htpp(',"pformid":"'||escapeRest(PFORMID)||'"');
    htpp(',"gbuttonsave":"'||escapeRest(GBUTTONSAVE)||'"');
    htpp(',"gbuttoncompile":"'||escapeRest(GBUTTONCOMPILE)||'"');
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"');
    htpp(',"gbuttonprev":"'||escapeRest(GBUTTONPREV)||'"');
    htpp(',"error":"'||escapeRest(ERROR)||'"');
    htpp(',"message":"'||escapeRest(MESSAGE)||'"');
    htpp(',"warning":"'||escapeRest(WARNING)||'"');
    htpp(',"btnback":"'||escapeRest(BTNBACK)||'"');
    htpp(',"hintcontent":"'||escapeRest(HINTCONTENT)||'"');
    htpp('},');
    if ShowBlockb10_DIV then
    htpp('"b10":[');
  v_firstrow__ := true;
  for i__ in 1..
B10NAME
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b10name":"'||escapeRest(B10NAME(i__))||'"');
    htpp(',"b10version":"'||escapeRest(B10VERSION(i__))||'"');
    htpp(',"b10git":"'||escapeRest(B10GIT(i__))||'"');
    htpp(',"b10description":"'||escapeRest(B10DESCRIPTION(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp('"b10":[]');
  end if;
    if ShowBlockb20_DIV then
    htpp(',"b20":[');
  v_firstrow__ := true;
  for i__ in 1..
B20LINKX
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b20linkx":"'||escapeRest(B20LINKX(i__))||'"');
    htpp(',"b20title":"'||escapeRest(B20TITLE(i__))||'"');
    htpp(',"b20datex":"'||escapeRest(B20DATEX(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b20":[]');
  end if;
    htpp('}}');
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_VERSIONS',ACTION);
  if ACTION is null then null;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

--<POST_ACTION formid="103" blockid="">
--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
--</POST_ACTION>
    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| RASDI_TRNSLT.text('RASD versions',LANG)||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head><body><div id="RASDC2_VERSIONS_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_VERSIONS_LAB',''|| RASDI_TRNSLT.text('RASD versions',LANG)||'') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASDC2_VERSIONS' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_VERSIONS_FOOTER',1,instr('RASDC2_VERSIONS_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end;
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_VERSIONS',ACTION);
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

--<POST_ACTION formid="103" blockid="">
--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
--</POST_ACTION>
-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end;
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_VERSIONS',ACTION);
  if ACTION is null then null;
    RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

--<POST_ACTION formid="103" blockid="">
--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
--</POST_ACTION>
-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASDC2_VERSIONS" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('{"form":{"@name":"RASDC2_VERSIONS","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end;
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>103</formid><form>RASDC2_VERSIONS</form><version>1</version><change>28.02.2024 12/44/40</change><user>RASDDEV</user><label><![CDATA[<%= RASDI_TRNSLT.text(''RASD versions'',LANG)%>]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>Y</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>28.09.2023 07/19/57</change><compileyn>Y</compileyn><application>RASD 2.0</application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks><block><blockid>B10</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[select distinct object_name from user_objects o

where o.object_name like ''RASD%''

and o.object_type = ''PACKAGE''

order by 1]]></sqltext><label></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10</blockid><fieldid>DESCRIPTION</fieldid><type>L</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>30</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10DESCRIPTION</nameid><label><![CDATA[<%= showLabel(''Description'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>GIT</fieldid><type>C</type><format></format><element>IMG_</element><hiddenyn></hiddenyn><orderby>25</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10GIT</nameid><label></label><linkid>link$togit</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field>';
 v_vc(2) := '<blockid>B10</blockid><fieldid>NAME</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>10</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10NAME</nameid><label><![CDATA[<%= showLabel(''Name'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>VERSION</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>20</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10VERSION</nameid><label><![CDATA[<%= showLabel(''Version'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B20</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[from (

select

  0 vr,

  ''D - ''||extractvalue(value(a), ''//item/title'' ) title,

  extractvalue(value(a),''//item/link'') linkx,

  extractvalue(value(a),''//item/pubDate'') datex

from table (xmlsequence

           (extract( xmltype( v_forum

              )

              , ''//rss/channel/item''

           ))) a

where rownum < 5

union all

SELECT 1 vr, ''G - ''||nvl(title,''NaN'') title, linkx, datex

FROM

JSON_TABLE(v_gitd , ''$[*]''

COLUMNS

  (title VARCHAR2(100) PATH ''$.commit.message'',

   linkx VARCHAR2(100) PATH ''$.html_url'',

   datex VARCHAR2(100) PATH ''$.commit.author.date''

   )

  )

where rownum < 5

union all

select

  1 vr,

  ''S - ''||nvl(extractvalue(value(a), ''//item/title'' ),''NaN'') title,

  extractvalue(value(a),''//item/link'') linkx,

  extractvalue(value(a),''//item/pubDate'') datex

from table (xmlsequence

           (extract( xmltype( v_project

              )

              , ''//rss/channel/item''

           ))) a

where rownum < 5       ';
 v_vc(3) := '

union all

select

 2 vr,

  ''F - ''||extractvalue(value(a), ''//item/title'' ) title,

  extractvalue(value(a),''//item/link'') linkx,

  extractvalue(value(a),''//item/pubDate'') datex

from table (xmlsequence

           (extract( xmltype( v_code

              )

              , ''//rss/channel/item''

           ))) a

where rownum < 3

)]]></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B20</blockid><fieldid>DATEX</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B20DATEX</nameid><label><![CDATA[Date of change]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B20</blockid><fieldid>LINKX</fieldid><type>C</type><format></format><element>A_</element><hiddenyn></hiddenyn><orderby>30</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B20LINKX</nameid><label><![CDATA[Link]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B20</blockid><fieldid>TITLE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>40</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B20TITLE</nameid><label><![CDATA[Last changes of RASD]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pky';
 v_vc(4) := 'n>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ACTION</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>BTNBACK</fieldid><type>C</type><format></format><element>INPUT_BUTTON</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Back'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>BTNBACK</nameid><label></label><linkid>link$back</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>COMPID</fieldid><type>N</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>COMPID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ERROR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCLR</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><delet';
 v_vc(5) := 'eyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONCLR'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONCLR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCLR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>13</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Compile'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONCOMPILE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCOMPILE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONPREV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>15</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Preview'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONPREV</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONPREV</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONRES</fieldid><type>C</type><format></format><element>INPUT_RESET</element><hiddenyn></hiddenyn><orderby>14</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Reset'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONRES</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONRES</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><type>C</type><format></format><element>INPU';
 v_vc(6) := 'T_SUBMIT</element><hiddenyn></hiddenyn><orderby>12</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Save'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSAVE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>9</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Search'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSRC</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>HINTCONTENT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>HINTCONTENT</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>HINTCONTENT</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>LANG</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>5</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>LANG</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>LANG</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldi';
 v_vc(7) := 'd><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>51</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>MESSAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[0]]></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>7</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORM</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>6</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORMID</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB10</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hi';
 v_vc(8) := 'ddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMP</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMP</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>UNLINK</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>UNLINK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VLOB</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>81</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VLOB</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VLOB</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VUSER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>80</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</not';
 v_vc(9) := 'nullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VUSER</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VUSER</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>V_CODE</fieldid><type>L</type><format></format><element></element><hiddenyn></hiddenyn><orderby>3</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>V_CODE</nameid><label><![CDATA[v_code]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>V_FORUM</fieldid><type>L</type><format></format><element></element><hiddenyn></hiddenyn><orderby>3</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>V_FORUM</nameid><label><![CDATA[v_forum]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>V_GITD</fieldid><type>L</type><format></format><element></element><hiddenyn></hiddenyn><orderby>3</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>V_GITD</nameid><label><![CDATA[v_gitd]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>V_PROJECT</fieldid><type>L</type><format></format><element></element><hiddenyn></hiddenyn><orderby>3</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>V_PROJECT</nameid><label><![CDATA[v_project]]></label><linkid></';
 v_vc(10) := 'linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>52</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>WARNING</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>XRFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>XRFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields><links><link><linkid>link$back</linkid><link>BACK</link><type>F</type><location><![CDATA[I]]></location><text></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>#JSLINK#2</paramid><type>OUT</type><orderby>2</orderby><blockid></blockid><fieldid>#JSLINK#</fieldid><namecid></namecid><code></code><value><![CDATA[history.go(-1);]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$togit</linkid><link>2GIT</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!rasdc2_git.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>20</paramid><type>OUT</type><orderby>20</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code></code><value><![CDATA[PGROUP=rasd-tool-20]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>21</paramid><type>OUT</type><orderby>21</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code></code><value><![CDATA[POWNER=<%= USER%>]';
 v_vc(11) := ']></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>B10NAME18</paramid><type>OUT</type><orderby>18</orderby><blockid>B10</blockid><fieldid>NAME</fieldid><namecid>POBJECT</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>LANG19</paramid><type>OUT</type><orderby>19</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link></links><pages><pagedata><page>0</page><blockid></blockid><fieldid>PF</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata>';
 v_vc(12) := '<pagedata><page>0</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>VUSER</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>VLOB</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>COMPID</';
 v_vc(13) := 'fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>XRFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>XRFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata></pages><triggers><trigger><blockid></blockid><triggerid>FORM_CSS</triggerid><plsql><![CDATA[.rasdTxB10DESCRIPTION {

    height: 70px;

    display: grid;

    overflow-y: scroll;

    font-size: x-small;

}



#GBUTTONRES_RASD {

 display: none;

}



#GBUTTONPREV_RASD  {

 display: none;

}
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS</triggerid><plsql><![CDATA[$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)









 });
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS_REF(82)</triggerid><plsql><![CDATA[$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("<%= RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) %>");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></t';
 v_vc(14) := 'rigger><trigger><blockid>GBUTTONPREV</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>GBUTTONPREV</rblockid></trigger><trigger><blockid>HINTCONTENT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>HINTCONTENT</rblockid></trigger><trigger><blockid></blockid><triggerid>POST_ACTION</triggerid><plsql><![CDATA[--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
]]></plsql><plsqlspec><![CDATA[  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then
    raise_application_error(''-20000'', ''ACTION="''||ACTION||''" is not defined. Define it in POST_ACTION trigger.'');
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>POST_SELECT</triggerid><plsql><![CDATA[declare

  x1 clob;

  y1 varchar2(100);

  ';
 v_vc(15) := '

begin

execute immediate ''

declare

 x clob;

 y varchar2(100);

begin

 :y := ''||B10NAME||''.version(x);

 :o := x;

end;''

using out y1, out x1

;



B10VERSION := y1;

B10DESCRIPTION := x1;

exception when others then



declare

  x1 clob;

  y1 varchar2(100);



begin

execute immediate ''

declare

 x clob;

 y varchar2(100);

begin

 :y := ''||B10NAME||''.changes(x);

 :o := x;

end;''

using out y1, out x1

;



B10VERSION := y1;

B10DESCRIPTION := x1;

exception when others then



B10VERSION := '''';

B10DESCRIPTION := ''error'';



end;





end;



B10DESCRIPTION := replace(B10DESCRIPTION,chr(10),''<br/>'');



if instr(rasd_client.secSuperUsers, VUSER) > 0 then

   B10GIT := ''rasdc_files.showfile?pfile=pict/gumbgit.jpg'';

end if;


]]></plsql><plsqlspec><![CDATA[-- Triggers after executing SQL. Use (i__) to access fields values.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B20</blockid><triggerid>POST_SELECT</triggerid><plsql><![CDATA[B20LINKX#SET.custom := ''href="''||B20LINKX||''" target="_blank"'';
]]></plsql><plsqlspec><![CDATA[-- Triggers after executing SQL. Use (i__) to access fields values.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_SUBMIT</triggerid><plsql><![CDATA[----put procedure in the begining of trigger;

post_submit_template;


]]></plsql><plsqlspec><![CDATA[-- Executing after filling fields on submit.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid>B20</blockid><triggerid>PRE_SELECT</triggerid><plsql><![CDATA[declare

     req   UTL_HTTP.REQ;

     resp  UTL_HTTP.RESP;

     value VARCHAR2(1024);

	 v_url_project varchar2(1000) := ''https://sourceforge.net/p/rasd/code-0/feed'';

	 v_url_code varchar2(1000) := ''https://sourceforge.net/projects/rasd/rss?path=/'';

	 v_url_git varchar2(1000) := ''https://api.github.com/repos/DomenDolar/RASD/commits'';

	 v_url_forum varchar2(1000) := ''https://sourceforge.net/p/rasd/discussion/hintsandtipsrasd/feed.rss'';

begin

 --   utl_http.set_wallet( rasdi_client.c_WalletLocation , rasdi_client.c_WalletPwd);

 utl_http.set_wallet( '''' );



/*    req := UTL_HTTP.BEGIN_REQUEST(''https://sourcef';
 v_vc(16) := 'orge.net/p/rasd/code-0/feed'');

    UTL_HTTP.SET_HEADER(req, ''User-Agent'', ''Mozilla/4.0'');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_LINE(resp, value, TRUE);

      v_project := v_project || value;

    END LOOP;

    UTL_HTTP.END_RESPONSE(resp);

*/

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(v_url_project);

      for i in 1 .. pieces.count loop

        v_project := v_project || pieces(i);



      end loop;

    end;



/*

    req := UTL_HTTP.BEGIN_REQUEST(''https://sourceforge.net/projects/rasd/rss?path=/'');

    UTL_HTTP.SET_HEADER(req, ''User-Agent'', ''Mozilla/4.0'');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_LINE(resp, value, TRUE);

      v_code := v_code || value;

    END LOOP;

    UTL_HTTP.END_RESPONSE(resp);

*/

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(v_url_code);

      for i in 1 .. pieces.count loop

        v_code := v_code || pieces(i);



      end loop;

    end;



/*    req := UTL_HTTP.BEGIN_REQUEST(''https://sourceforge.net/p/rasd/discussion/hintsandtipsrasd/feed.rss'');

    UTL_HTTP.SET_HEADER(req, ''User-Agent'', ''Mozilla/4.0'');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_LINE(resp, value, TRUE);

      v_forum := v_forum || value;

    END LOOP;

    UTL_HTTP.END_RESPONSE(resp);

*/

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(v_url_forum);

      for i in 1 .. pieces.count loop

        v_forum := v_forum || pieces(i);



      end loop;

    end;



    declare

     req   UTL_HTTP.REQ;

     resp  UTL_HTTP.RESP;

     value VARCHAR2(1024);

    begin

    req := UTL_HTTP.BEGIN_REQUEST(v_url_git);

    UTL_HTTP.SET_HEADER(req, ''User-Agent'', ''Mozilla/4.0'');

    resp := UTL_HTTP.GET_RESPONSE(req);

    LOOP

      UTL_HTTP.READ_TEXT(resp, value, 1000);

      v_gitd := v_gitd || value;

     END LOOP;

      UTL_HTTP.END_RESPONSE(resp);

    exception when UTL_HTTP.end_of_body then

      UTL_HTTP.END_RESPONSE(resp);

    end;





/*

    declare

      pieces utl_http.html_pieces;

    begin

      pieces := utl_http.request_pieces(''https://api.github.com/repos/DomenDolar/RASD/commits'');

      for i in 1 .. pieces.count loop

        v_gitd := v_gitd || piec';
 v_vc(17) := 'es(i);



      end loop;

    end;

*/







    /*

    select URIFACTORY.getUri(''https://sourceforge.net/p/rasd/code-0/feed'').getClob() ,

     URIFACTORY.getUri(''https://sourceforge.net/projects/rasd/rss?path=/'').getClob() ,

     URIFACTORY.getUri(''https://sourceforge.net/p/rasd/discussion/hintsandtipsrasd/feed.rss'').getClob()

     into v_project, v_code , v_forum

    from dual;

    */

  exception when others then

  v_project  := ''<?xml version="1.0" encoding="utf-8"?>

<rss><channel><item><title>RSS feed is not connected (''||sqlerrm||'') - show in browser</title><link>''||v_url_project||''</link><pubDate> </pubDate></item></channel></rss>'';

  v_code  := ''<?xml version="1.0" encoding="utf-8"?><rss><channel><item><title>Code SourceForge</title><link>''||v_url_code||''</link><pubDate> </pubDate></item><item><title>Code GIT</title><link>''||v_url_git||''</link><pubDate> </pubDate></item></channel></rss>'';

  v_forum  := ''<?xml version="1.0" encoding="utf-8"?>

<rss><channel><item><title>RSS forum feed is not connected (''||sqlerrm||'') - show in browser</title><link>''||v_url_forum||''</link><pubDate> </pubDate></item></channel></rss>'';

end;
]]></plsql><plsqlspec><![CDATA[-- Triggers before executing SQL.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>_ changes</triggerid><plsql><![CDATA[  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := ''/* Change LOG:

20210914 - Added new UTL_HTTP REQUEST, GIT aded to ACL

20201122 - Added program RASDC_WELCOME

20200928 - Added program RASDC_GIT

20160413 - Added RSS forum feed

20151202 - Included session variables in filters

20141027 - Added footer on all pages

*/'';

    return version;

 end;
]]></plsql><plsqlspec><![CDATA[  function changes(p_log out varchar2) return varchar2;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure custom template</triggerid><plsql><![CDATA[function showLabel(plabel varchar2, pcolor varchar2 default ''U'', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.link';
 v_vc(18) := 'Dialog(replace(replace(plabel,'' '',''''),''.'',''''),lang,replace(this_form,''2'','''')||''_DIALOG'');

end if;



if pcolor is null then



return v__;



else



return ''<font color="''||pcolor||''">''||v__||''</font>'';



end if;





end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure post_submit template</triggerid><plsql><![CDATA[procedure post_submit_template is

begin

/*

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;

*/



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text(''User has no privileges to save data!'', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure_compile_template</triggerid><plsql><![CDATA[procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, ''COMPILE_S'', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text(''From is not generated.'', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.en';
 v_vc(19) := 'gineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           ''begin '' || v_server || ''.c_debug := false;''|| v_server || ''.form('' || PFORMID ||

                           '','''''' || lang || '''''');end;'',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text(''From is generated.'', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || ''<br/> - ''||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text(''Form is generated with compilation error. Check your code.'', lang)||''(''||sqlerrm||'')'';



            else

            sporocilo := RASDI_TRNSLT.text(''Form is NOT generated - internal RASD error.'', lang) || ''(''||sqlerrm||'')<br>''||

                         RASDI_TRNSLT.text(''To debug run: '', lang) || ''begin '' || v_server || ''.form('' || PFORMID ||

                         '','''''' || lang || '''''');end;'' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, ''COMPILE_E'', pcompid);

end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecod';
 v_vc(20) := 'e><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''<%= RASDI_TRNSLT.text(''RASD versions'',LANG)%>''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');
%>]]></value><valuecode><![CDATA['');
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''''|| RASDI_TRNSLT.text(''RASD versions'',LANG)||''''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(';
 v_vc(21) := 'FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');

htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASDC2_VERSIONS</id><nameid>RASDC2_VERSIONS</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[?]]></value><valuecode><![CDATA[="?"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><va';
 v_vc(22) := 'lueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASDC2_VERSIONS]]></value><valuecode><![CDATA[="RASDC2_VERSIONS"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>RASDC2_VERSIONS_LAB</id><nameid>RASDC2_VERSIONS_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_LAB]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></';
 v_vc(23) := 'rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASDC2_VERSIONS_LAB'',''<%= RASDI_TRNSLT.text(''RASD versions'',LANG)%>'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASDC2_VERSIONS_LAB'',''''|| RASDI_TRNSLT.text(''RASD versions'',LANG)||'''') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASDC2_VERSIONS_MENU</id><nameid>RASDC2_VERSIONS_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_MENU]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><';
 v_vc(24) := 'valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASDC2_VERSIONS_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASDC2_VERSIONS_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>15</pelementid><orderby>9999</orderby><element>FORM_BOTTOM</element><type>F</type><id>RASDC2_VERSIONS_BOTTOM</id><nameid>RASDC2_VERSIONS_BOTTOM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBottom]]></value><valuecode><![CDATA[="rasdFormBottom"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_BOTTOM]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_BOTTOM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></';
 v_vc(25) := 'valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASDC2_VERSIONS_BOTTOM'',1,instr(''RASDC2_VERSIONS_BOTTOM'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASDC2_VERSIONS_BOTTOM'',1,instr(''RASDC2_VERSIONS_BOTTOM'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASDC2_VERSIONS_DIV</id><nameid>RASDC2_VERSIONS_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_DIV]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_DIV"]]></valuecode><for';
 v_vc(26) := 'loop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>20</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASDC2_VERSIONS_HEAD</id><nameid>RASDC2_VERSIONS_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_HEAD]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlob';
 v_vc(27) := 'id></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>20</pelementid><orderby>5</orderby><element>FORM_BODY</element><type>F</type><id>RASDC2_VERSIONS_BODY</id><nameid>RASDC2_VERSIONS_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_BODY]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>20</pelementid><o';
 v_vc(28) := 'rderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASDC2_VERSIONS_FOOTER</id><nameid>RASDC2_VERSIONS_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_FOOTER]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>20</pelementid><orderby>2</orderby><element>FORM_RESPONSE</element><type>F</type><id>RASDC2_VERSIONS_RESPONSE</id><nameid>RASDC2_VERSIONS_RESPONSE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormResponse]]></value><valuecode><![CDATA[="rasdFormR';
 v_vc(29) := 'esponse"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_RESPONSE]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_RESPONSE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>24</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASDC2_VERSIONS_MESSAGE</id><nameid>RASDC2_VERSIONS_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_MESSAGE]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><s';
 v_vc(30) := 'ource></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>24</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASDC2_VERSIONS_ERROR</id><nameid>RASDC2_VERSIONS_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_ERROR]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobi';
 v_vc(31) := 'd><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASDC2_VERSIONS_WARNING</id><nameid>RASDC2_VERSIONS_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_VERSIONS_WARNING]]></value><valuecode><![CDATA[="RASDC2_VERSIONS_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid';
 v_vc(32) := '><pelementid>22</pelementid><orderby>105</orderby><element>BLOCK_DIV</element><type>B</type><id>B10_DIV</id><nameid>B10_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_DIV]]></value><valuecode><![CDATA[="B10_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB10_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>28</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</typ';
 v_vc(33) := 'e><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10_LAB</id><nameid>B10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_LAB]]></value><valuecode><![CDATA[="B10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5<';
 v_vc(34) := '/orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>28</pelementid><orderby>106</orderby><element>TABLE_N</element><type></type><id>B10_TABLE</id><nameid>B10_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_TABLE_RASD]]></value><valuecode><![CDATA[="B10_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><so';
 v_vc(35) := 'urce></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>31</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10_BLOCK</id><nameid>B10_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_BLOCK_NAME]]></value><valuecode><![CDATA[="B10_BLOCK_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB10 in 1..B10NAME.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid><';
 v_vc(36) := '/rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>31</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B10_THEAD</id><nameid>B10_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>33</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>34</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B10NAME_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></sourc';
 v_vc(37) := 'e><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10NAME]]></value><valuecode><![CDATA[="rasdTxLabB10NAME"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>35</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10NAME_LAB</id><nameid>B10NAME_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderb';
 v_vc(38) := 'y>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10NAME_LAB]]></value><valuecode><![CDATA[="B10NAME_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Name'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Name'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>34</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B10VERSION_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[';
 v_vc(39) := 'rasdTxLabB10VERSION]]></value><valuecode><![CDATA[="rasdTxLabB10VERSION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>37</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>B10VERSION_LAB</id><nameid>B10VERSION_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10VERSION_LAB]]></value><valuecode><![CDATA[="B10VERSION_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></val';
 v_vc(40) := 'ueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Version'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Version'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>34</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>B10GIT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10GIT]]></value><valuecode><![CDATA[="rasdTxLabB10GIT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><att';
 v_vc(41) := 'ribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>3</orderby><element>FONT_</element><type>L</type><id>B10GIT_LAB</id><nameid>B10GIT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10GIT_LAB]]></value><valuecode><![CDATA[="B10GIT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddeny';
 v_vc(42) := 'n>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>34</pelementid><orderby>4</orderby><element>TX_</element><type>E</type><id></id><nameid>B10DESCRIPTION_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10DESCRIPTION]]></value><valuecode><![CDATA[="rasdTxLabB10DESCRIPTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>41</pelementid><orderby>4</orderby><element>FONT_</element><type>L</type><id>B10DESCRIPTION_LAB</id><nameid>B10DESCRIPTION_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribu';
 v_vc(43) := 'te><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10DESCRIPTION_LAB]]></value><valuecode><![CDATA[="B10DESCRIPTION_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Description'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Description'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>22</pelementid><orderby>115</orderby><element>BLOCK_DIV</element><type>B</type><id>B20_DIV</id><nameid>B20_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![C';
 v_vc(44) := 'DATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_DIV]]></value><valuecode><![CDATA[="B20_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB20_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB20_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>43</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></sour';
 v_vc(45) := 'ce><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>45</elementid><pelementid>44</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B20_LAB</id><nameid>B20_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_LAB]]></value><valuecode><![CDATA[="B20_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><';
 v_vc(46) := 'text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>46</elementid><pelementid>43</pelementid><orderby>116</orderby><element>TABLE_N</element><type></type><id>B20_TABLE</id><nameid>B20_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_TABLE_RASD]]></value><valuecode><![CDATA[="B20_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></';
 v_vc(47) := 'valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>47</elementid><pelementid>46</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B20_BLOCK</id><nameid>B20_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_BLOCK_NAME]]></value><valuecode><![CDATA[="B20_BLOCK_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB20 in 1..B20LINKX.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>48</elementid><pelementid>46</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B20_THEAD</id><nameid>B20_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</order';
 v_vc(48) := 'by><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>49</elementid><pelementid>48</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>50</elementid><pelementid>49</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B20LINKX_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidde';
 v_vc(49) := 'nyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20LINKX]]></value><valuecode><![CDATA[="rasdTxLabB20LINKX"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>51</elementid><pelementid>50</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B20LINKX_LAB</id><nameid>B20LINKX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20LINKX_LAB]]></value><valuecode><![CDATA[="B20LINKX_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attri';
 v_vc(50) := 'bute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[Link]]></value><valuecode><![CDATA[Link]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>52</elementid><pelementid>49</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B20TITLE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20TITLE]]></value><valuecode><![CDATA[="rasdTxLabB20TITLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></f';
 v_vc(51) := 'orloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>53</elementid><pelementid>52</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>B20TITLE_LAB</id><nameid>B20TITLE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20TITLE_LAB]]></value><valuecode><![CDATA[="B20TITLE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribu';
 v_vc(52) := 'te><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[Last changes of RASD]]></value><valuecode><![CDATA[Last changes of RASD]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>54</elementid><pelementid>49</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>B20DATEX_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20DATEX]]></value><valuecode><![CDATA[="rasdTxLabB20DATEX"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>55</elementid><pelementid>54</pelementid><orderby>3</orderby><element>FONT_</element><ty';
 v_vc(53) := 'pe>L</type><id>B20DATEX_LAB</id><nameid>B20DATEX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20DATEX_LAB]]></value><valuecode><![CDATA[="B20DATEX_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[Date of change]]></value><valuecode><![CDATA[Date of change]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>56</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10</id><nameid>RECNUMB10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></';
 v_vc(54) := 'rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10_NAME]]></value><valuecode><![CDATA[="RECNUMB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidd';
 v_vc(55) := 'en]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10_VALUE]]></value><valuecode><![CDATA[="''||RECNUMB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>57</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMP</id><nameid>RECNUMP</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></';
 v_vc(56) := 'attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMP_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMP_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMP_NAME]]></value><valuecode><![CDATA[="RECNUMP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMP_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMP))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></';
 v_vc(57) := 'value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>58</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><';
 v_vc(58) := 'rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(PAGE))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>59</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><end';
 v_vc(59) := 'tagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribut';
 v_vc(60) := 'e>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>60</elementid><pelementid>21</pelementid><orderby>6</orderby><element>INPUT_HIDDEN</element><type>D</type><id>LANG</id><nameid>LANG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid';
 v_vc(61) := '><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[LANG_NAME_RASD]]></value><valuecode><![CDATA[="LANG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[LANG_NAME]]></value><valuecode><![CDATA[="LANG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[LANG_VALUE]]></value><valuecode><![CDATA[="''||LANG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute';
 v_vc(62) := '><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>61</elementid><pelementid>21</pelementid><orderby>7</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PFORMID</id><nameid>PFORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PFORMID_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFORMID_NAME]]></value><valuecode><![CDATA[="PFORMID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><te';
 v_vc(63) := 'xtid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFORMID_VALUE]]></value><valuecode><![CDATA[="''||PFORMID||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>62</elementid><pelementid>21</pelementid><orderby>13</orderby><element>INPUT_SUBMIT</eleme';
 v_vc(64) := 'nt><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></te';
 v_vc(65) := 'xt><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![';
 v_vc(66) := 'CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobi';
 v_vc(67) := 'd><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>63</elementid><pelementid>23</pelementid><orderby>10011</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7)';
 v_vc(68) := ' ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLIC';
 v_vc(69) := 'K</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><val';
 v_vc(70) := 'ueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn>';
 v_vc(71) := '<valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>64</elementid><pelementid>21</pelementid><orderby>14</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><at';
 v_vc(72) := 'tribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><typ';
 v_vc(73) := 'e>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><valu';
 v_vc(74) := 'e><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>65<';
 v_vc(75) := '/elementid><pelementid>23</pelementid><orderby>10012</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode>';
 v_vc(76) := '</textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><';
 v_vc(77) := 'rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SE';
 v_vc(78) := 'T.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>66</elementid><pelementid>21</pelementid><orderby>15</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTT';
 v_vc(79) := 'ONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rl';
 v_vc(80) := 'obid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></val';
 v_vc(81) := 'uecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><';
 v_vc(82) := 'forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>67</elementid><pelementid>23</pelementid><orderby>10013</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute>';
 v_vc(83) := '<attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CD';
 v_vc(84) := 'ATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuec';
 v_vc(85) := 'ode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>68</elementid><pelementid>21</pelementid><orderby>16</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></sour';
 v_vc(86) := 'ce><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute';
 v_vc(87) := '><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><';
 v_vc(88) := 'textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>69</elementid><pelementid>23</pelementid><orderby>10014</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><texti';
 v_vc(89) := 'd></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.e';
 v_vc(90) := 'rror||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: oran';
 v_vc(91) := 'ge;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>70</elementid><pelementid>26</pelementid><orderby>10046</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid>';
 v_vc(92) := '</attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><val';
 v_vc(93) := 'ueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>71</elementid><pelementid>25</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value';
 v_vc(94) := '></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>72</elementid><pelementid>27</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A';
 v_vc(95) := '_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><te';
 v_vc(96) := 'xtcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>73</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>INPUT_BUTTON</element><type>D</type><id>BTNBACK</id><nameid>BTNBACK</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><val';
 v_vc(97) := 'uecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[BTNBACK_NAME_RASD]]></value><valuecode><![CDATA[="BTNBACK_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[BTNBACK_NAME]]></value><valuecode><![CDATA[="BTNBACK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[link$back]]></value><valuecode><![CDATA[="javascript: history.go(-1);"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[BTNBACK_VALUE]]></value><valuecode><![CDATA[="''||BTNBACK||''"]]></valuecode><forloop></forloop><endloop></endloop><source></so';
 v_vc(98) := 'urce><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldBTNBACK  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>74</elementid><pelementid>23</pelementid><orderby>300874</orderby><element>INPUT_BUTTON</element><type>D</type><id>BTNBACK</id><nameid>BTNBACK</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[BTNBACK_NAME_RASD]]></value><valuecode><![CDATA[="BTNBACK_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</';
 v_vc(99) := 'orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[BTNBACK_NAME]]></value><valuecode><![CDATA[="BTNBACK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[link$back]]></value><valuecode><![CDATA[="javascript: history.go(-1);"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[BTNBACK_VALUE]]></value><valuecode><![CDATA[="''||BTNBACK||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldBTNBACK  then
htp.prn(''<input]]></name><valu';
 v_vc(100) := 'e><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>75</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>76</elem';
 v_vc(101) := 'entid><pelementid>23</pelementid><orderby>300874</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>77</elementid><pelementid>32</pelementid><orderby>20</orderby><element>TX_</element><type>E</type><id></id><nameid>B10NAME_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_C';
 v_vc(102) := 'LASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10NAME rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10NAME rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10NAME_NAME]]></value><valuecode><![CDATA[="rasdTxB10NAME_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>78</elementid><pelementid>77</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B10NAME</id><nameid>B10NAME</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop';
 v_vc(103) := '></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10NAME_NAME_RASD]]></value><valuecode><![CDATA[="B10NAME_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text><';
 v_vc(104) := '/text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B10NAME_VALUE]]></value><valuecode><![CDATA[''||B10NAME(iB10)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>79</elementid><pelementid>32</pelementid><orderby>40</orderby><element>TX_</element><type>E</type><id></id><nameid>B10VERSION_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10VERSION rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10VERSION rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10VERSION_NAME]]></value><valuecode><![CDATA[="rasdTxB10VERSION_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></f';
 v_vc(105) := 'orloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>80</elementid><pelementid>79</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B10VERSION</id><nameid>B10VERSION</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10VERSION_NAME_RASD]]></value><valuecode><![CDATA[="B10VERSION_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby>';
 v_vc(106) := '<attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B10VERSION_VALUE]]></value><valuecode><![CDATA[''||B10VERSION(iB10)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></te';
 v_vc(107) := 'xtid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>81</elementid><pelementid>32</pelementid><orderby>50</orderby><element>TX_</element><type>E</type><id></id><nameid>B10GIT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10GIT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10GIT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10GIT_NAME]]></value><valuecode><![CDATA[="rasdTxB10GIT_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>82</elementid><pelementid>81</pelementid><orderby>1</orderby><element>IMG_</element><type>D</type><id>B10GIT</id><nameid>B10GIT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>11</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDA';
 v_vc(108) := 'TA[CLASS]]></name><value><![CDATA[rasdImg]]></value><valuecode><![CDATA[="rasdImg"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>9</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[B10GIT_NAME_RASD]]></value><valuecode><![CDATA[="B10GIT_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[link$togit]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$togit(B10GIT(iB10),''B10GIT_''||iB10||'''');
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>A_SRC</attribute><type>A</type><text></text><name><![CDATA[SRC]]></name><value';
 v_vc(109) := '><![CDATA[B10GIT_VALUE]]></value><valuecode><![CDATA[="''||B10GIT(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  B10GIT_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  B10GIT(iB10) is not null and '''' is not null  then htp.prn(''title=""''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</IMG]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<IMG]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>83</elementid><pelementid>32</pelementid><orderby>60</orderby><element>TX_</element><type>E</type><id></id><nameid>B10DESCRIPTION_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10DESCRIPTION rasdTxTypeL]]>';
 v_vc(110) := '</value><valuecode><![CDATA[="rasdTxB10DESCRIPTION rasdTxTypeL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10DESCRIPTION_NAME]]></value><valuecode><![CDATA[="rasdTxB10DESCRIPTION_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>84</elementid><pelementid>83</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B10DESCRIPTION</id><nameid>B10DESCRIPTION</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></va';
 v_vc(111) := 'lueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10DESCRIPTION_NAME_RASD]]></value><valuecode><![CDATA[="B10DESCRIPTION_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><va';
 v_vc(112) := 'luecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% htpClob( ''B10DESCRIPTION_VALUE'' ); %>]]></value><valuecode><![CDATA['');  htpClob( ''''||B10DESCRIPTION(iB10)||'''' );
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>85</elementid><pelementid>47</pelementid><orderby>60</orderby><element>TX_</element><type>E</type><id></id><nameid>B20LINKX_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20LINKX rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20LINKX rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20LINKX_NAME]]></value><valuecode><![CDATA[="rasdTxB20LINKX_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endlo';
 v_vc(113) := 'op></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>86</elementid><pelementid>85</pelementid><orderby>1</orderby><element>A_</element><type>D</type><id>B20LINKX</id><nameid>B20LINKX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[href]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20LINKX_NAME_RASD]]></value><valuecode><![CDATA[="B20LINKX_''||iB20||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B20LINKX_NAME]]></value><valuecode><![CDATA[="B20LINKX_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><at';
 v_vc(114) := 'tribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  B20LINKX_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  B20LINKX(iB20) is not null and '''' is not null  then htp.prn(''title=""''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B20LINKX#SET(iB20).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B20LINKX#SET(iB20).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfor';
 v_vc(115) := 'm><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B20LINKX#SET(iB20).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B20LINKX#SET(iB20).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B20LINKX#SET(iB20).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B20LINKX#SET(iB20).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B20LINKX#SET(iB20).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B20LINKX#SET(iB20).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B20LINKX#SET(iB20).error is not null then htp.p('' title="''||B20LINKX#SET(iB20).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B20LINKX#SET(iB20).error is not null then htp.p('' title="''||B20LINKX#SET(iB20).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B20LINKX#SET(iB20).info is not null then htp.p('' title="''||B20LINKX#SET(iB20).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B20LINKX#SET(iB20).info is not null then htp.p('' tit';
 v_vc(116) := 'le="''||B20LINKX#SET(iB20).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</a><% end if; %>]]></value><valuecode><![CDATA[</a>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B20LINKX#SET(iB20).visible then %><a]]></value><valuecode><![CDATA['');  if B20LINKX#SET(iB20).visible then
htp.prn(''<a]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20LINKX_VALUE]]></value><valuecode><![CDATA[''||B20LINKX(iB20)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>87</elementid><pelementid>47</pelementid><orderby>80</orderby><element>TX_</element><type>E</type><id></id><nameid>B20TITLE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20TITLE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20TITLE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20TITLE_NAME]]></value><valueco';
 v_vc(117) := 'de><![CDATA[="rasdTxB20TITLE_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>88</elementid><pelementid>87</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B20TITLE</id><nameid>B20TITLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20TITLE_NAME_RASD]]></value><valuecode><![CDATA[="B20TITLE_''||iB20||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlob';
 v_vc(118) := 'id></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hi';
 v_vc(119) := 'ddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20TITLE_VALUE]]></value><valuecode><![CDATA[''||B20TITLE(iB20)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>89</elementid><pelementid>47</pelementid><orderby>100</orderby><element>TX_</element><type>E</type><id></id><nameid>B20DATEX_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20DATEX rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20DATEX rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20DATEX_NAME]]></value><valuecode><![CDATA[="rasdTxB20DATEX_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></a';
 v_vc(120) := 'ttributes></element><element><elementid>90</elementid><pelementid>89</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B20DATEX</id><nameid>B20DATEX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20DATEX_NAME_RASD]]></value><valuecode><![CDATA[="B20DATEX_''||iB20||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</';
 v_vc(121) := 'hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20DATEX_VALUE]]></value><valuecode><![CDATA[''||B20DATEX(iB20)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASDC2_VERSIONS_v.1.1.20240228124440.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASDC2_VERSIONS;

/
